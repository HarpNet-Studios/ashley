# Ashley, the Crimson build/distribution script

import os, sys, random
import time, datetime
import subprocess
import argparse

logfile = open("ashley."+str(int(time.time()))+".log", "x", encoding='utf-8')

_version = "0.1dev"
_starttime = str(datetime.datetime.now().ctime())

# Important globals
printdbg = False

b_dbg = True
b_push = False

#d_appid = 

laststatus = None

# Colorama init
from colorama import init, Fore, Back, Style
init(autoreset=True)

# Clear screen
def cls(): os.system('cls' if os.name=='nt' else 'clear')
cls()

## Functions

# Log
def slog(string, status=None):
	st = ""

	if   status is None: st = "[*] "
	elif status == -2: st = ""
	elif status == -1: st = "[....] "
	elif status ==  0: st = "[ OK ] "
	elif status ==  1: st = "[FAIL] "
	elif status ==  2: st = "[INFO] "
	elif status ==  3: st = "[TODO] "
	else: st = "[UNK?] "

	logfile.write(st+string+"\n")

# Print and Log
def plog(string, status=None, nolog=False):
	global laststatus
	st = ""

	r = Style.BRIGHT+Fore.RED
	g = Style.BRIGHT+Fore.GREEN
	b = Style.BRIGHT+Fore.BLUE
	y = Style.BRIGHT+Fore.YELLOW
	o = Fore.YELLOW
	x = Style.BRIGHT+Fore.BLACK
	rt = Style.RESET_ALL

	if not nolog: slog(string, status)

	if   status is None: st = "["+y+"*"+rt+"] "
	elif status == -2: st = ""
	elif status == -1: st = "["+x+"...."+rt+"] "
	elif status ==  0: st = "["+g+" OK "+rt+"] "
	elif status ==  1: st = "["+r+"FAIL"+rt+"] "
	elif status ==  2: st = "["+b+"INFO"+rt+"] "
	elif status ==  3: st = "["+o+"TODO"+rt+"] "
	else: st = "["+y+"UNK?"+rt+"] "

	if (status is None) and printdbg:
		print(st+string)
	elif status is not None: 
		print(st+string, end="\r" if not printdbg else "\n")
		if status != -1: sys.stdout.write("\n")
	laststatus = status

def pdiv():
	plog("",-2)

# Run external process
def exr(log,args,lineout=False,c=False,discord=False):
	crucial = c
	plog(log, -1)
	with subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) as proc:
		lns = 0
		while True:
			output = proc.stdout.readline().decode('utf-8')
			if output == '' and proc.poll() is not None:
				break
			if output:
				lns += 1
				# Discord stuff here?
				plog(output.rstrip())
			if not printdbg:
				plog(log + " ["+Style.BRIGHT+Fore.BLACK+str(lns)+Style.RESET_ALL+"]", -1, True)
			proc.poll()
		rc = proc.returncode
		plog(log, rc)
		if rc != 0 and crucial:
			plog("SOMETHING BROKE! Check log for details!",1)
			exit(1)
		return rc

# Creates header
def hdr(strings, pad, type=0, color=Fore.BLUE):
	ok = ""
	if   type == 0: ok = "==="
	elif type == 1: ok = "="
	# ---- #
	elif type == -1: ok = "#"
	elif type == -2: ok = "##"
	elif type == -3: ok = "###"

	if isinstance(strings, list):
		str = ""
		for i in strings:
			str += color+ok+" "+i+" "+color+ok+"\n"
	else:
		str = color+ok+" "+strings+" "+color+ok+("\n" if pad else "")
	
	slog(ok+" "+strings+" "+ok+("\n" if pad else ""), -2)
	return str

# Creates and prints a header
def phdr(strings, pad=True, type=0, color=Fore.BLUE):
	print(hdr(strings, pad, type, color))

# Creates a prompt
def prompt(string, default=None, color=Back.RED):
	valid = {"y": True, "n": False}

	prompt = ""
	if default is None: prompt = "[y/n]"
	elif default == "y": prompt = "[Y/n]"
	elif default == "n": prompt = "[y/N]"
	else: raise ValueError("Invalid default answer: '%s'" % default)

	while True:
		#sys.stdout.write()
		choice = input("\n"+Back.WHITE+" "+color+" "+string+" "+Fore.BLACK+Back.WHITE+prompt+Style.RESET_ALL+" ").lower()
		if default is not None and choice == '':
			print()
			slog("\n  "+string+" "+prompt+" [DEFAULT]"+"\n", -2)
			return valid[default]
		elif choice in valid:
			print()
			slog("\n  "+string+" "+prompt+" "+choice+"\n", -2)
			return valid[choice]
		else:
			sys.stdout.write(Fore.RED+"Invalid selection."+"\n")

# Discord push function
def discordPush():
	exr("Test login status",["dispatch","login"])
	ss = exr("Pushing build to Discord",["dispatch","build","push","623616609952464936","config.json","discord"],discord=True)
	build_id = ss[1]


	status = ss[0]
	return status

## Main loop

try:
	phdr("Ashley, the builder ("+_version+")", False)
	phdr("By Yellowberry")

	phdr("Started at "+_starttime, False, 1, Fore.GREEN)

	printdbg = prompt("Provide debug output?","n")

	exr("Testing exr() with 'butler version'",['butler','version'])

	b_dbg = prompt("Debug build?","y")
	if not b_dbg: # Release selected
		relconf = prompt("Are you sure?","n",Back.BLUE)
		if not relconf: # Allow a backup from an accidental keypress
			b_dbg = True

	b_type = ("Debug" if b_dbg else "Release")

	plog("Building "+b_type+" build!",2)

	b_arch = []

	b_arch_32 = prompt("32-bit build?","y")
	if b_arch_32: b_arch.append("Win32")

	b_arch_64 = prompt("64-bit build?","y")
	if b_arch_64: b_arch.append("x64")

	if b_dbg: # Debug build	
		for i in b_arch:
				run = [
				"C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\amd64\\MSBuild.exe",
				"../../C++/Cardboard-rewrite/src/vcpp/cardboard.sln",
				"/p:Configuration="+b_type,
				"/p:Platform="+i
				]

				exr("Building with Visual Studio (%s|%s)" % (b_type, i), run, c=True)

	else: # Release build

		gamever = input("bruh, version number: ");

		b_push = prompt("Push to markets?","y")

		if b_push:
			phdr("itch.io",True,1,Fore.MAGENTA)

			exr("Validating Crimson build folder",["butler", "validate", "../../C++/Cardboard-discord/discord/"], c=True)
			b_stat_itch = exr("Pushing Crimson changes",["butler", "push", "--if-changed", "--dry-run", "../../C++/Cardboard-discord/discord/", "harpnetstudios/project-crimson:win-alpha", "--userversion="+gamever], c=True)
			
			pdiv()

			phdr("Discord",True,1,Fore.MAGENTA)
			plog("TODO: Haha no.",3)
			#b_stat_discord = discordPush()
			
			pdiv()

			phdr("Steam",True,1,Fore.MAGENTA)

			plog("TODO: Implement this.",3)
			plog("Oh yeah, and also get the game on Steam in the first place.",3)

			pdiv()

			plog("Update: itch.io",b_stat_itch)
			#plog("Update: Discord",b_stat_discord)
			plog("Update: Steam",3)

	#exr("")
	pdiv()
	plog("All done! At "+str(datetime.datetime.now().ctime()), 0)
except KeyboardInterrupt:
	print()
	plog("Interrupted by user.", 1)
	exit(1)